var animeSpeed = 200;


// =======================================
// = Startup functions on document ready =
// =======================================
jQuery(document).ready(function() {
    visualsInit();
    backTop();
    slideshowInit();
    menuEffects();
    stripDropdown();
    portfolioFilters();
});

// ====================================
// = Startup functions on window load =
// ====================================
jQuery(window).load(function() {
    jQuery('.slideshow-slider').slideshow();
});


// ===============================
// = Initialize visuals function =
// ===============================
function visualsInit() {
        /* Tabs Activiation
    ================================================== */
    var tabs = jQuery('ul.tabs');
    tabs.each(function(i) {
        //Get all tabs
        var tab = jQuery(this).find('> li > a');
        tab.on('click', function(e) {

            var link = jQuery(this);
            var li = link.parent();
            var index = li.index();
            var container = link.parents('.tab-container');
            var content = container.find('.tabs-content').find('li:eq(' + index + ')');
            e.preventDefault();
            tab.removeClass('active');
            link.addClass('active');
            content.slideToggle('animeSpeed').siblings().slideUp(animeSpeed);

        });
    });


    /* Accordion
    ================================================== */
    jQuery('.accordion').find('header:not(.active)').next().hide();
    jQuery('.accordion header').click(function(){
        var header = jQuery(this);
        var accordion = header.parents('.accordion');

        if ( !header.hasClass('active') ) {
            var active = accordion.find('.active');
            active.next().slideUp();
            active.removeClass('active');
            // fixing active in ie7
            active.find('span').toggle();
        }
        header.toggleClass('active');
        // fixing other headers in ie7
        header.find('span').toggle();
        header.parent().find('.accordion-content').slideToggle(animeSpeed);
        return false;
      });

    /* Toggles
    ================================================== */
    var toggles = jQuery('.no-details').find('details');
    toggles.each(function() {
        var toggle = jQuery(this);
        var summary = toggle.find('summary');
        var contents = toggle.find('.contents');

        if (!toggle.attr('open')) {
            contents.hide();
        }
        summary.prepend(contents.is(':visible') ? '<span>-</span>' : '<span>+</span>');
        toggle.click(function(){
            contents.slideToggle('animeSpeed', function() {
                contents.is(':visible') ? summary.find('span').text('-') : summary.find('span').text('+');
            });
        });
        // $this.find('span').text($(this).is(':visible') ? '-' : '+');
    });

    /* Author Toggle
    ================================================== */
    jQuery('.author-more').on('click', function() {
        jQuery(this).next().slideToggle('animeSpeed');
    });

    /* Responsive Menu
    ================================================== */
    jQuery( '#access .menu' ).mobileMenu();

    /* Icon fonts
    ================================================== */
    jQuery('[data-icon]').each(function(){
        var target = jQuery(this);
        var icon = jQuery('<b>' + target.attr('data-icon') + '</b>');
        icon.attr('aria-hidden', 'true');
        target.prepend(icon);
    });
    /* Fancybox 2
    ================================================== */
    jQuery( '.fancybox' ).fancybox({
        padding:  5,
        helpers: {
            overlay : { opacity: 0.9 },
            title : { type : 'inside' }
        }
    });

    jQuery( '.fancybox_vimeo' ).live( 'click', function( e ) {
        jQuery.fancybox({
            padding: 5,
            width: 700,
            height: 394,
            title: this.title,
            type: "iframe",
            href: this.href.replace( 'vimeo.com', 'player.vimeo.com/video' ),
            helpers: {
                overlay : { opacity: 0.9 },
                title : { type : 'inside' }
            }
       });
       e.preventDefault();
    });

    jQuery( '.fancybox_youtube' ).live( "click", function( e ) {
        jQuery.fancybox({
            padding: 5,
            width: 700,
            height: 394,
            title: this.title,
            type: 'iframe',
            href: this.href.replace( 'youtube.com/watch?v=', 'youtube.com/embed/' ),
            helpers: {
                overlay : { opacity: 0.9 },
                title : { type : 'inside' }
            }
       });
       e.preventDefault();
    });

    jQuery( '.fancybox_gallery' ).fancybox({
        padding: 5,
        //autoPlay: true,
        helpers: {
            overlay: { opacity: 0.9 },
            title   : { type : 'inside' },
            thumbs  : {
                width   : 50,
                height  : 50
            }
        }
    });
}

// ====================
// = Back to top link =
// ====================
function backTop() {
    var backtop = jQuery('#back-top');
    backtop.hide();
    jQuery(window).scroll( function() {
        if ( jQuery(this).scrollTop() > 100 ) {
            backtop.fadeIn();
        } else {
            backtop.fadeOut();
        }
    });
    backtop.on('click', function(e) {
        jQuery('body,html').animate({ scrollTop: 0 }, animeSpeed);
        e.preventDefault();
    });
}

// =============================
// = Dropdowns effect for menu =
// =============================
function menuEffects() {
    jQuery('#access ul li').hover(function() {
        jQuery(this).find(' > ul').hide().slideDown(animeSpeed);
    }, function() {
        jQuery(this).find(' > ul').slideUp(animeSpeed);
    });
}

// =============================
// = Dropdowns effect for strip =
// =============================
function stripDropdown() {
    var topStrip = jQuery('#top-strip');
    var stripToggle = jQuery('#toggle-strip');
    stripToggle.on('click', function(e) {
       topStrip.slideToggle(animeSpeed);
       stripToggle.toggleClass('hidden');
       if (stripToggle.hasClass('hidden')) {
            stripToggle.find('b').text(';');
       } else {
            stripToggle.find('b').text(':');
       }
       e.preventDefault();
    });
}

// ================================
// = Initialize Slideshow function =
// =================================
function slideshowInit() {
    jQuery('.slideshow').preloader();
    jQuery('.slideshow figcaption p').lettering('lines');
}

// ================================
// = Portfolio filters =
// =================================
function portfolioFilters() {
    var filters = jQuery('.portfolio-container .filters').find('a');
    filters.each(function() {
        var filter = jQuery(this);
        filter.on('click', function(e) {
            var active = jQuery(this);
            var filters = active.parents('.filters');
            var portfolio = filters.next();
            var activeClass = active.attr('class').replace('filter-','');
            portfolio.find('.inactive').removeClass('inactive');

            filters.find('a').removeClass('active');
            active.addClass('active');

            if ( activeClass != 'all') {
                portfolio.find('li:not(.' + activeClass + ')').addClass('inactive');
            };
            e.preventDefault();
        });
    });
}



