(function($){
    $.fn.slideshow = function(){
        return this.each(function() {
            var $this = $(this);                           
            $this.find('.flexslider').flexslider({
                animation: $this.attr('data-anim'),
                slideDirection: $this.attr('data-slidedir'),
                slideshowSpeed: $this.attr('data-speed'),
                animationDuration: $this.attr('data-duration'),
                controlNav: 'true' === $this.attr('data-controlnav'),
                directionNav: 'true' === $this.attr('data-directionnav'),
                randomize: 'true' === $this.attr('data-random'),
                controlsContainer: '.flex-container',
                prevText: "<",
                nextText: ">",
                start : function(slider) {
                    // Hide the preloader
                    $this.find('.loading').hide(200, function() { jQuery(this).remove();});
                    // Add class to be used as shadow
                    $this.addClass('slideshadow');
                    slider.slides.eq(0).addClass('active');
                        if (jQuery('html').hasClass('ie7')) {
                            var controls = slider.parent().find('.flex-control-nav');
                            fixiebullets(controls);
                        }
                    },
                after : function(slider) {
                    slider.find('.active').removeClass('active');
                    slider.slides.eq(slider.currentSlide).addClass('active');
                }
            });
        });
    };      
    $.fn.preloader = function() {
        this.prepend('<span class="loading"></span>');
        var i = 0;
        setInterval(function() {
            jQuery('.loading').text(i++);
            i = (i > 7) ? 0 : i++;
        }, 100);
    };
    $.fn.portfolio = function(){
        return this.each(function() {
            var $this = $( this );  
            var flexslider = $this;
            var flexheader = $this.prev( 'header' );
            var filter = '';
            var folio = flexslider.find( '.slides' );
            var readMore = $this.attr( 'data-readmore' );
            var numItems = $this.attr( 'data-num-items' );             
            folio.remove('.embed-container');
            var portfolio = portfolioMaker(folio,'', 'grid', numItems , '');
    
            portfolioGridTrunk(folio, readMore);
    
            flexslider.find( '.flexslider' ).flexslider( {
               animation: "slide",
               slideshow: false,
               controlsContainer: '.flex-container',
               prevText: "<",
               nextText: ">",
               start: function(slider) { 
                    // fix the font icon bullets in ie7
                    if (jQuery('html').hasClass('ie7')) {
                        var controls = slider.parent().find('.flex-control-nav');
                        fixiebullets(controls);      
                    }
            
                    flexheader.find( '.grid-list' ).find('a').off( 'click' );
                    flexheader.find( '.grid-list' ).find('a').on( 'click', function(e) {
                      var link = jQuery( this );
                      if( ! link.hasClass( 'active') ) {
                
                        flexheader.find( '.grid-list' ).find('a').removeClass( 'active' );
                        link.addClass( 'active' );
                        if ( link.hasClass('singleview')) {
                            var newportfolio = portfolioMaker(portfolio, filter, '', '', readMore);
                            var oldClass = 'grid-view';
                            var newClass = 'slide-view';
                        } else {
                            var newportfolio = portfolioMaker(portfolio, filter, 'grid', numItems, readMore);
                            var oldClass = 'slide-view';
                            var newClass = 'grid-view';
                            if (jQuery('html').is('.ie7, .ie8')) {
                                filterfix(newportfolio);    
                            }
                        }
                        portfolioSwitch( flexslider, slider, newportfolio, oldClass, newClass );
                      }
                      e.preventDefault();
                    });

                    flexheader.find('.filters' ).find('a').off( 'click' );
                    flexheader.find('.filters' ).find('a').on( 'click', function(e) {
                      var view = flexheader.find('.grid-list').find('a.active').attr('class').replace('active', '').replace(' ', '');
                      filter = jQuery(this).attr('href').replace('#', '');
                      if ( view == 'gridview') {
                        filtered = portfolioMaker(portfolio, filter, view, numItems, readMore);
                        if (jQuery('html').is('.ie7, .ie8')) {
                            filterfix(filtered);
                        }
                      } else {
                          filtered = portfolioMaker(portfolio, filter, '', '', readMore);
                      }
                      portfolioSwitch( flexslider, slider, filtered, view, view );
                      e.preventDefault();
                    });
               }   
                      
           });
            // =======================
            // = Portfolio functions =
            // =======================

            /* Switching between different portfolios
            ================================================== */
            function portfolioSwitch( flexslider, slider, newHTML, oldClass, newClass ) {    

                // fade out the old portfolio and fadein the new when its ready
                flexslider.animate({opacity: 0}, 500, function() {
                    slider.html( newHTML );
                    flexslider.removeClass(oldClass).addClass(newClass);
                    slider.find( '.clone' ).remove();
                    // remove nav and controls
                    slider.next().remove();
                    slider.next().remove();
                    slider.init();  
                }).animate({opacity: 1}, 1000);
            }

            /* Trunkate the figcaption description to fit in the grid icons
            ================================================================ */
            function portfolioGridTrunk(grid, more) {
                grid.find('figure').each( function() {
                    var figure = jQuery(this);
                    var link = figure.find('a:first-child').attr('href');
                    var caption = figure.find('figcaption');
                    var title = figure.find('h3').first().html();
                    var text = caption.find( 'p' ).first().text();
                    if ( text || text!='') {
                       text = text.substring(0, 75) + '&hellip;';
                    };
                    caption.html( '<h3>' + title + '</h3><p>' + text + '</p><a href="' + link + '" class="more-link">' + more + '</a>');
                });
            }

            /* Make a new portfolio using the filters , view and gridno
            =========================================================== */
            function portfolioMaker(portfolio, filters, view, gridno, more) {
                // Set filters 
                if ( filters == '' || !filters ) {
                    filters = '.portfolio-item';
                } else {
                    filters = '.portfolio-item.' + filters;
                }
                view = view || 'list';
                var sourcelist = portfolio.find( filters );
                var list = jQuery( '<ul class="slides"></ul>' );
                sourcelist.each(function() {
                    var listitem = jQuery(this);
                    var link = listitem.find('figure>:first-child').clone();
                    var href = link.attr('href');
                    var figcaption = listitem.find('figcaption').html();
                    if ( more != '' ) {
                        figcaption = figcaption + '<a href="' + href + '" class="more-link">' + more + '</a>';    
                    }
                    var image = link.html();
                    var classes = listitem.attr('class');
                    list.append('<li class="' + classes + '"><figure><a href="' + href + '">' + image + '</a><figcaption>' + figcaption + '</figcaption></figure></li>');
                });
                if ( view =='list' ) {
                    // if a slide-view portfolio is requested return the simple list
                    return list;
                } else {
                    // else create a new grid using the portfolio items thats in the list
                    var grid = jQuery( '<ul class="slides"></ul>' );
                    var liset = jQuery.makeArray(list.children('li'));
                    var items = liset.length;
                    for (var pages = 0; pages < Math.ceil(items / gridno) ; pages++) {
                        var li = jQuery( '<li><div class="portfolio"><ul class="portfolio-list"></ul></div></li>' );
                        startli = pages*gridno;
                        endli = ((pages + 1)*gridno < items) ? (pages + 1)*gridno : items;
                        for (var m = startli; m < endli; m++) {
                            li.find('.portfolio-list').append(liset[m]);
                        };
                        grid.append(li);
                    };
                    if ( more != '' ) {
                        portfolioGridTrunk(grid, more);    
                    }
                    return(grid);
                }
            }                                     

        });
    };     
})(jQuery);             
